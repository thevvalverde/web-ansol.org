# Como contribuir para o sítio Web da ANSOL

## Descrição da imagem de cabeçalho nas notícias

Para adicionar alt text e uma legenda à imagem de capa das notícias, pode-se
adicionar os seguintes campos de metadados ao ficheiro markdown:

```
---
image:
  alt_text: "Descrição da imagem"
  caption: "Texto que aparece por baixo da imagem, normalmente a creditar a sua autoria"
---
```


## Novo evento para a agenda:

Os eventos são adicionados à agenda criando um novo ficheiro em formato
markdown [content/eventos/](./content/eventos). Pode ser adicionado um ficheiro
que servirá de ícone na lista dos eventos, idealmente no formato svg.

### Instruções

1. criar uma directoria em `content/eventos/`, dedicada ao evento (por exemplo "content/eventos/AAAA-MM-DD-megahackaton").
2. na sub-directoria de evento deverá ser criado um ficheiro chamado index.md, com o conteúdo como este template de exemplo:
   ```
   ---
   layout: evento
   title: Nome do evento
   metadata:
     event:
       location: Local Mega fixe, Rua super-cool, nº 1 2A, 9999-999 Cidade
       site:
         url: https://website.do.evento.org/
       date:
         start: AAAA-MM-DD
         finish: AAAA-MM-DD
   ---

   Descrição textual do evento, possivelmente com outras imagens (que devem ser
   adicionadas à mesma directoria deste ficheiro), como por exemplo uma imagem de cartaz:

   ![Cartaz](cartaz.png)
   ```
3. caso haja uma imagem de ícone para o evento, esta deve ser colocada na directoria do evento e ter o nome `cover` (por exemplo: cover.svg, ou cover.png).

Exemplo de layout de ficheiros para um evento na agenda:
```
content/eventos/
  2024-01-01-megahackaton/
    cartaz.png
    cover.svg
    index.md
```

**Atenção**: não é recomendado utilizar imagens de cartaz como "cover", porque
irá aparecer num quadrado pequeno na lista de eventos. Quando houver imagens de cartaz,
o ideal é adicionar o cartaz à directoria e incluí-la na descrição do evento, em markdown.


# Licença

Como podem ver no ficheiro [LICENSE](./LICENSE), o sítio Web da ANSOL está licenciado com a licença GNU Affero General Public License Version 3 (AGPLv3).
