---
layout: evento
title: Festa do Software Livre 2023
metadata:
  event:
    date:
      start: 2023-09-15
      finish: 2023-09-17
    location: Universidade de Aveiro
    site:
      url: https://softwarelivre.eu
---

Reservem já no vosso calendário, de dia 15 a dia 17 vamos organizar mais uma Festa de Software Livre.

Três dias de apresentações, workshops, e outras actividades relacionadas com Software Livre.

**Mais informações em breve**
