---
categories:
- linux kernel
- libre software
metadata:
  mapa:
  - mapa_geom: !binary |-
      AQEAAAAAAAAA0EoiwKwvz4z3XENA
    mapa_geo_type: point
    mapa_lat: !ruby/object:BigDecimal 27:0.38726304627576e2
    mapa_lon: !ruby/object:BigDecimal 27:-0.9146118164063e1
    mapa_left: !ruby/object:BigDecimal 27:-0.9146118164063e1
    mapa_top: !ruby/object:BigDecimal 27:0.38726304627576e2
    mapa_right: !ruby/object:BigDecimal 27:-0.9146118164063e1
    mapa_bottom: !ruby/object:BigDecimal 27:0.38726304627576e2
    mapa_geohash: eyckrcgvpfrvrupb
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 320
  - tags_tid: 316
  node_id: 661
  event:
    location: Lisbon, Portugal
    site:
      title: Linux Plumbers Conference
      url: https://www.linuxplumbersconf.org/
    date:
      start: 2019-09-09 00:00:00.000000000 +01:00
      finish: 2019-09-11 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Linux Plumbers Conference
created: 1552181630
date: 2019-03-10
aliases:
- "/evento/661/"
- "/node/661/"
---
<p>This conference is for plumbing-basis system analysis and knowledge exchange with regard to the Linux Kernel. Developers are invited to hold talks about the Linux utility code for all purposes. The 3-day-event will be broadcast on Youtube after it has finished.</p>
