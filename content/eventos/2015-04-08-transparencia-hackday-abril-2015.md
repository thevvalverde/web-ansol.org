---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 305
  event:
    location: 
    site:
      title: ''
      url: http://www.transparenciahackday.org/2015/04/dia-11-de-abril-o-hackday-e-ao-contrario/
    date:
      start: 2015-04-11 00:00:00.000000000 +01:00
      finish: 2015-04-11 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Transparência HackDay - Abril 2015
created: 1428522429
date: 2015-04-08
aliases:
- "/evento/305/"
- "/node/305/"
---
<div class="entry-content"><p>Ainda a recuperar do sempre oportuno dia 1 de abril, esse dia que uma vez por ano nos faz avaliar criticamente tudo o que é dito e publicado, estamos a preparar um hackday inovador e disruptivo.</p><p>Vamos pensar em tudo ao contrário: dados que são mentira, informações irrelevantes, correlações inoportunas, conclusões apressadas, apps sem propósito, webservices disfuncionais, visualizações confusas, estatísticas trapaceiras, datasets crípticos, plataformas descolaborativas, cidadania 0.2, redes anti-sociais, metodologias pouco ágeis e modelos de negócio insustentáveis. As nossas inspirações para esta experiência em inutilidade são as Stupid Hackathons (em <a href="http://www.stupidhackathon.com/">Nova Iorque</a> e <a href="https://stupidhackathon.github.io">São Francisco</a>).</p><p>Tens uma ideia inovadora? Deixa-a em casa! Dia 11 de abril está reservado para conceitos enviesados e propostas totós. Temos muitos datasets para explorar e imaginar a “next small thing”!</p><p>Vai ser um dia sério, mesmo que não seja a sério! Anda daí e vem deixar o mundo na mesma!</p><p>11 de abril, no <strong>Pavilhão-Jardim</strong> do <a href="http://uptec.up.pt/uptec/polo-das-industrias-criativas" target="_blank" title="Parque de Ciência e Tecnologia da Universidade do Porto">Pólo de Indústrias Criativas</a> (UPTEC PINC), no horário habitual: <strong>11:00—13:00</strong> + <strong>14:30—17:30</strong>.</p></div>
