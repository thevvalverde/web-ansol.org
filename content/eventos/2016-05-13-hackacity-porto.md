---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 421
  event:
    location: 
    site:
      title: ''
      url: http://porto.hackacity.eu/
    date:
      start: 2016-05-27 00:00:00.000000000 +01:00
      finish: 2016-05-28 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Hackacity Porto
created: 1463154157
date: 2016-05-13
aliases:
- "/evento/421/"
- "/node/421/"
---
<div id="element-custom-block-129983346" class="element "><div id="sfid-129983346" class="sf content headline "><div class="title grp p0"><p>Uma nova edição do épico hackathon que tem como objetivo melhorar a vida de quem vive ou visita o Porto está a chegar.<br>Uma maratona de 24 horas de trabalho em equipa com algumas surpresas... e acima de tudo muita diversão e aprendizagem!</p></div></div></div>
