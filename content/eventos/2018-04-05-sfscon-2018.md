---
categories:
- foss
- day
- meeting
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 160
  - tags_tid: 286
  - tags_tid: 129
  node_id: 602
  event:
    location: Bozen, South Tyrol, Italia
    site:
      title: SFScon
      url: https://www.sfscon.it/
    date:
      start: 2018-11-16 00:00:00.000000000 +00:00
      finish: 2018-11-16 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: SFScon 2018
created: 1522941628
date: 2018-04-05
aliases:
- "/evento/602/"
- "/node/602/"
---
<p>The <strong>South Tyrol Free Software Conference (SFScon)</strong> is an event of <strong>international stature</strong> that attracts more and more visitors every year from Northern Italy, Austria and Switzerland.</p><p>It is an <strong>annual conference dedicated to Free Software</strong> in South Tyrol that is aimed at the general public, but in particular at decision-makers in the public administration and business worlds. SFScon promotes the use of Free Software in IT infrastructures as a tool to achieve greater innovation and competitiveness in the region.</p><p>Thanks to the presence of speakers with national and international reputations, the conference is a meeting place for experts, end users and those interested in Free Software, offering participants the opportunity to share best practice and to get to know the latest innovations in the field.</p><p>Founded in 2001 at the initiative of the Linux User Group Bozen-Bolzano-Bulsan, SFScon has since 2005 been jointly organised and promoted by the TIS innovation park in Bolzano, now IDM Südtirol – Alto Adige. From 2004 onwards the Linux User Group Bozen-Bolzano-Bulsan has been presenting the “South Tyrol Free Software Award” (SFS Award) at SFScon.</p>
