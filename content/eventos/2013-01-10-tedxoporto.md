---
categories: []
metadata:
  node_id: 119
  event:
    location: Porto
    site:
      title: ''
      url: http://tedxoporto.com/
    date:
      start: 2013-04-13 00:00:00.000000000 +01:00
      finish: 2013-04-13 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: TEDxO'Porto
created: 1357818395
date: 2013-01-10
aliases:
- "/evento/119/"
- "/node/119/"
---
<p>&nbsp;</p>
<div>
	O tema deste evento &eacute; &quot;Em Fus&atilde;o&quot;. Para resolver novos problemas temos de ir al&eacute;m das&nbsp;solu&ccedil;&otilde;es do costume. Para criar valor verdadeiramente distintivo temos de&nbsp;procurar novas &aacute;reas e disciplinas de conhecimento e mexer, cruzar e&nbsp;misturar. Temos de ser mais ousados! Neste dia ir-se-&agrave; explorar a riqueza da&nbsp;cultura open-source e o seu efeito em diversas &aacute;reas tal como nas artes,&nbsp;cultura, ci&ecirc;ncia, sociedade e tecnologia. Vamos ver como a partilha e&nbsp;colabora&ccedil;&atilde;o pode potenciar novos conceitos e compreender como o passado nos&nbsp;projetar&aacute; no futuro atrav&eacute;s da reinven&ccedil;&atilde;o!</div>
