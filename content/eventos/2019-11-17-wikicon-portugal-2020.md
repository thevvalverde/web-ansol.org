---
categories: []
metadata:
  mapa:
  - mapa_geom: !binary |-
      AQEAAAAAAAAQwjohwD1g9RcikkRA
    mapa_geo_type: point
    mapa_lat: !ruby/object:BigDecimal 27:0.41141665453732e2
    mapa_lon: !ruby/object:BigDecimal 27:-0.8614761829376e1
    mapa_left: !ruby/object:BigDecimal 27:-0.8614761829376e1
    mapa_top: !ruby/object:BigDecimal 27:0.41141665453732e2
    mapa_right: !ruby/object:BigDecimal 27:-0.8614761829376e1
    mapa_bottom: !ruby/object:BigDecimal 27:0.41141665453732e2
    mapa_geohash: ez3f5cwgt6rczvxu
  slide:
  - slide_value: 0
  node_id: 714
  event:
    location: Casa do Infante, Porto
    site:
      title: ''
      url: https://meta.wikimedia.org/wiki/WikiCon_Portugal
    date:
      start: 2020-01-17 00:00:00.000000000 +00:00
      finish: 2020-01-19 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: WikiCon Portugal 2020
created: 1574017927
date: 2019-11-17
aliases:
- "/WikiCon2020/"
- "/evento/714/"
- "/node/714/"
---
<p>Por ocasião dos 19 anos da Wikipédia e dos 10 anos da Wikimédia Portugal, o <strong>WikiCon Portugal</strong> 2020 quer celebrar a comunidade que tomou para si a missão de "cada ser humano poder partilhar livremente a soma de todo o conhecimento". O objectivo desta primeira edição do WikiCon Portugal é o desenvolvimento de capacidades da comunidade.</p><p>Entre outras apresentações, a ANSOL irá estar presente, e falar sobre "A Wikimedia e o dia do Domínio Público".</p>
