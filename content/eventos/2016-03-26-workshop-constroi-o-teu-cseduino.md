---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 408
  event:
    location: Lisboa
    site:
      title: ''
      url: http://altlab.org/2016/03/22/workshop-constroi-o-teu-cseduino-2/
    date:
      start: 2016-04-02 10:00:00.000000000 +01:00
      finish: 2016-04-02 18:00:00.000000000 +01:00
    map: {}
layout: evento
title: Workshop – Constrói o teu CSEduino
created: 1459030732
date: 2016-03-26
aliases:
- "/evento/408/"
- "/node/408/"
---
<p>No próximo dia 2 de Abril de 2016 vai realizar-se a Workshop – Constrói o teu <em>CSEduino</em>.</p><p>O <em>CSEduino</em> é uma versão barata do Arduino construída apenas com componentes <em>THT (Thru Hole Technology)</em> que são facilmente soldáveis a uma <em>PCB</em> desenhada especificamente para este projeto. Será apresentada nesta Workshop a versão 4 desta placa.</p><p>Esta workshop foi desenhada para ser acessível a qualquer pessoa que deseje dar os primeiros passos no mundo da electrónica e ter logo a mão um <em>kit</em> que te permite iniciar a programação de microcontroladores e a interacção com componentes diversos.</p><p>A Workshop realiza-se nas instalações do FabLab Lisboa – Rua Maria da Fonte – Mercado do Forno do Tijolo, 1170-221 Lisboa.</p><p>O custo da Workshop é de 30€ e inclui todos os componentes para a construção do <em>CSEduino</em> assim como cerca de 30 componentes adicionais que serão teus no final da <em>workshop</em> e que te permitirão fazer desde logo&nbsp;as tuas experiências.</p><p>A agenda da Workshop é a seguinte:</p><ul><li>Aprendendo a soldar;</li><li>Montagem do <em>CSEduino;</em></li><li>Quatro módulos que exploram os componentes que serão distribuídos no <em>kit</em>. Nestes quatro módulos iremos construir um <em>Ohmmeter</em>, um <em>Theremin</em>, um gerador de código Morse e mais algumas surpresas;</li><li>Algumas dicas sobre onde comprar mais componentes e como;</li></ul><p>O horário da Workshop é das 10H até às 18H com intervalo para almoço.</p><p>Nº Máximo de Participantes: 8</p><p>É necessário trazer um computador portátil com Linux&nbsp;ou Windows&nbsp;com uma porta USB disponível.</p><p>Todas as ferramentas para a montagem do <em>CSEduino</em> e dos módulos estarão disponíveis.<br> Qualquer questão adicional poderá ser colocada através de email para jpralves@gmail.com</p><p>Cada participante levará consigo um kit que lhe permitirá construir na workshop o CSEduino e todos os módulos. Será igualmente fornecido no <em>kit</em> uma placa com <em>interface</em>&nbsp;<em>USB</em> que permite programar o <em>CSEduino</em>.</p><p><a href="http://goo.gl/forms/dcVZDd1pY8" target="_blank" title="inscrição na Workshop">Link para inscrição na Workshop</a>.</p><p>A reserva dos lugares é feita por ordem de chegada e pagamento.</p><p>ATENÇÃO:</p><p>O pagamento da workshop poderá ser feito por transferência bancária para o seguinte NIB: 0035 0739 00027858130 36 ou para o IBAN: PT50 0035 0739 0002 7858 1303 6</p><p>O registo apenas será considerado válido após recepção do pagamento/comprovativo.</p><p>O comprovativo deverá ser enviado para workshops@altlab.org, apenas nesse momento será feito o registo com sucesso e receberás um email a confirmar a tua presença.</p>
