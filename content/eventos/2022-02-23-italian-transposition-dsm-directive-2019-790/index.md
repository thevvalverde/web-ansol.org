---
layout: evento
title: "The Italian transposition of the DSM Directive 2019/790"
metadata:
  event:
    date:
      start: 2022-02-23 09:30:00
      finish: 2022-02-23 11:00:00
    location: Online
    site:
      url: https://sites-twobirds.vuture.net/72/10932/february-2022/bird-and-bird-webinar---the-italian-transposition-of-the-dsm-directive-2019-790--shaping-a-digital-future-for-italy-and-europe-(1).asp
---

> In 2019 the EU legislature adopted its Directive on copyright in the Digital
> Single Market (DSM Directive 2019/790). Member States had until 7 June 2021
> to transpose that Directive into their own laws. Italy completed its
> transposition process in November 2021.
>
> The Italian transposition of the DSM Directive 2019/790 presents several
> important features, ranging from the way in which press publishers will be
> able to exercise their new related right to the articulation of online
> platforms' obligations under the regime for online content sharing service
> providers, and the harmonization of some aspects of contractual relations.
>
> This online event (in English) will provide an opportunity to discuss the
> principal innovations introduced into Italian copyright law as a consequence
> of transposing the DSM Directive 2019/790 and their practical application.
