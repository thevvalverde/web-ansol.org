---
categories:
- diy
- cseduino
- arduino
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 57
  - tags_tid: 58
  - tags_tid: 59
  node_id: 281
  event:
    location: 
    site:
      title: ''
      url: http://altlab.org/2015/02/03/workshop-constroi-o-teu-cseduino/
    date:
      start: 2015-02-28 00:00:00.000000000 +00:00
      finish: 2015-03-01 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: Workshop — Constrói o teu CSEduino
created: 1423413435
date: 2015-02-08
aliases:
- "/evento/281/"
- "/node/281/"
---
<p>Nos pró­xi­mos dias 28 de Feve­reiro e 1 de Março de 2015 vai realizar-se a Workshop — Cons­trói o teu <em>CSE­duino</em>.</p><p>O <em>CSE­duino</em> é uma ver­são barata do Arduino cons­truída ape­nas com com­po­nen­tes <em>THT (Thru Hole Tech­no­logy)</em> que são facil­mente sol­dá­veis a uma <em>Strip­bo­ard</em>. Será apre­sen­tada nesta Workshop a ver­são 2 desta&nbsp;placa.</p><p>Esta workshop foi dese­nhada para ser aces­sí­vel a qual­quer pes­soa que deseje dar os pri­mei­ros pas­sos no mundo da elec­tró­nica e ter logo a mão um <em>kit</em> que te per­mite ini­ciar a pro­gra­ma­ção de micro­con­tro­la­do­res e a inte­rac­ção com com­po­nen­tes diversos.</p><p>A Workshop realiza-se nas ins­ta­la­ções do FabLab Lis­boa — Rua Maria da Fonte – Mer­cado do Forno do Tijolo, 1170-221 Lisboa.</p><p>O custo da Workshop é de 60€ e inclui todos os com­po­nen­tes para a cons­tru­ção do <em>CSE­duino</em> assim como cerca de 30 com­po­nen­tes adi­ci­o­nais que serão teus no final da <em>workshop</em> e que te per­mi­ti­rão fazer desde logo&nbsp;as tuas experiências.</p>
