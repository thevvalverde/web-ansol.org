---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 175
  event:
    location: Lisboa
    site:
      title: ''
      url: http://www.eurodig.org/eurodig-2013/programme
    date:
      start: 2013-06-20 00:00:00.000000000 +01:00
      finish: 2013-06-21 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Internet For Society – How To Serve The Public Interest?
created: 1370454126
date: 2013-06-05
aliases:
- "/evento/175/"
- "/node/175/"
---

