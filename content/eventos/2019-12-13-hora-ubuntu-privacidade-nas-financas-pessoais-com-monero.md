---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 719
  event:
    location: Chalet 12, Sintra
    site:
      title: ''
      url: https://www.meetup.com/ubuntupt/events/266888646/
    date:
      start: 2019-12-19 18:30:00.000000000 +00:00
      finish: 2019-12-19 18:30:00.000000000 +00:00
    map: {}
layout: evento
title: Hora Ubuntu - Privacidade nas finanças pessoais com Monero
created: 1576245765
date: 2019-12-13
aliases:
- "/evento/719/"
- "/node/719/"
---

