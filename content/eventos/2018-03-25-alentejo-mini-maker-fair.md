---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 559
  event:
    location: Arena de Évora, Évora
    site:
      title: Alentejo Mini Maker Fair
      url: https://alentejo.makerfaire.com/
    date:
      start: 2018-05-11 00:00:00.000000000 +01:00
      finish: 2018-05-12 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Alentejo Mini Maker Fair
created: 1522018353
date: 2018-03-25
aliases:
- "/evento/559/"
- "/node/559/"
---
<p><br><br>A Maker Faire é um encontro de pessoas fascinantes e curiosas que gostam de aprender e adoram partilhar o que fazem. De engenheiros a artistas, de cientistas a artesãos, a Maker Faire um local para esses "makers" mostrarem os passatempos, experiências e projectos.<br><br>É o maior espetáculo de Mostra (e Conta) do planeta - uma demostração familiar de invenções, criatividade e engenho.<br><br>Veja o futuro e inspire-se.<br><br></p>
