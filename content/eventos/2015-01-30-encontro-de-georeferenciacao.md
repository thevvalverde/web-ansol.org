---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 276
  event:
    location: Águeda
    site:
      title: ''
      url: http://all.cm-agueda.pt/home/workshop-arduino/?SubsiteID=1#.VMtmL_4o7rE
    date:
      start: 2015-01-31 14:00:00.000000000 +00:00
      finish: 2015-01-31 17:00:00.000000000 +00:00
    map: {}
layout: evento
title: Encontro de Georeferenciação
created: 1422616226
date: 2015-01-30
aliases:
- "/evento/276/"
- "/node/276/"
---
<div>Título:</div><div>Workshop “georreferenciação”</div><div>------------------------------------------------------------</div><div>dia 31 de janeiro, das 14h às 17h na sala polivalente da biblioteca municipal manuel alegre</div><div>&nbsp;</div><div>Integrado no ciclo de workshops do ALL&nbsp;</div><div>------------------------------------------------------------</div><div>&nbsp;</div><div><div>O próximo workshop do ALL, dinamizado por Jorge Gustavo Rocha e António José Silva, está já agendado para dia 31 de janeiro, pelas 14h00 na sala polivalente da biblioteca municipal manuel alegre em Águeda. A participação é gratuita, mas os lugares são limitados. Inscreva-se hoje mesmo! Os participantes devem trazer computador portátil e um telemóvel com GPS.</div><div>&nbsp;</div><div>local de realização: Sala polivalente - biblioteca municipal manuel alegre - Águeda</div><div>email: <a href="mailto:all@cm-agueda.pt" target="_blank">all@cm-agueda.pt</a></div><div>duração do evento: 3 horas</div><div>&nbsp;</div><div>período de pré-inscrição entre os dias 24 e 30 de Janeiro de 2015 das 00:00 às 23:59</div></div><div>&nbsp;</div>
