---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 517
  event:
    location: 
    site:
      title: ''
      url: https://direitosdigitais.pt/comunicacao/22-noticias/27-inscricoes-abertas-the-school-of-rock-ing-eu-copyright
    date:
      start: 2017-10-20 00:00:00.000000000 +01:00
      finish: 2017-10-21 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: The School of Rock(ing) EU Copyright
created: 1501967714
date: 2017-08-05
aliases:
- "/evento/517/"
- "/node/517/"
---
<p><img src="https://direitosdigitais.pt/images/eventos/schoolofrockingcopyright/RockCopy_insc_v2.png" alt="School of Rock(in) EU Copyright" title="School of Rock(in) EU Copyright" style="display: block; margin-left: auto; margin-right: auto;" height="656" width="893"></p><p>Está actualmente em discussão na União Europeia uma reforma da legislação de direitos de autor. Estas reformas são raras, os seus efeitos costumam perdurar por muitos anos e as consequências das decisões que forem tomadas terão um impacto directo na vida de todos os cidadãos.</p><p>Em cooperação com a EDRi, COMMUNIA e Wikimedia, a D3 está a organizar um workshop dedicado à reforma europeia do direito de autor, os seus desafios, perigos e oportunidades.</p><p>O que pensas de vir a ter todos os teus memes, textos de blog ou vídeos privados filtrados por uma máquina europeia de censura? O que achas de não poderes fazer cópias dos teus próprios conteúdos culturais? De que forma o direito de autor afecta a liberdade de expressão no nosso dia a dia? E acima de tudo, o que podemos fazer para, juntos, mudar esta situação em que multinacionais falam em nome dos autores e limitam os nossos direitos fundamentais?</p><p>Os objectivos da School of Rock(ing) EU Copyright passam por “abanar” os participantes e por formar uma equipa de “copyfighters” para lutar pelos direitos dos utilizadores na agenda legislativa europeia. Vamos debater sobre quais são os assuntos mais preocupantes para o interesse público e sobre o que podemos fazer para trazer o tema para o espaço público, de forma a chegar aos cidadãos e políticos, quer a nível europeu, quer a nível nacional.</p><p>The School of Rock(ing) EU Copyright decorrerá em Lisboa, nos dias 20 e 21 de Outubro de 2017 (final de tarde de sexta-feira e Sábado durante todo o dia).<br>O workshop é destinado a cerca de 15 participantes, sendo leccionado em inglês. Está disponível apoio financeiro para despesas de deslocação e alojamento.</p><p>As inscrições já estão abertas.</p>
