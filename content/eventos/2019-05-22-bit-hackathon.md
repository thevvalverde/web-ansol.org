---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 674
  event:
    location: Porto
    site:
      title: ''
      url: https://bithackathon.sonae.pt/
    date:
      start: 2019-05-30 00:00:00.000000000 +01:00
      finish: 2019-05-31 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: BIT Hackathon
created: 1558558041
date: 2019-05-22
aliases:
- "/evento/674/"
- "/node/674/"
---

