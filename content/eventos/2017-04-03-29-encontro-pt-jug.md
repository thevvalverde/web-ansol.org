---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 496
  event:
    location: " Pavilhão de Civil do IST, Piso -1, Sala VA1., Lisboa"
    site:
      title: 29ºEncontro PT.JUG
      url: https://www.meetup.com/pt-BR/pt-jug/events/238834265/?eventId=238834265
    date:
      start: 2017-04-10 17:30:00.000000000 +01:00
      finish: 2017-04-10 17:30:00.000000000 +01:00
    map: {}
layout: evento
title: 29º encontro PT.JUG
created: 1491237179
date: 2017-04-03
aliases:
- "/evento/496/"
- "/node/496/"
---
﻿On the next April, 10th, we are happy to have Olivier Prouvost from OPCoach to talk about the latest Eclipse technologies. 

Abstract
Eclipse is not only an IDE for developing in Java. It is also an eco-system, managed by a Foundation and providing a lot of different projects that can help you to increase your productivity whatever the project and the programming language you use. 
This presentation will give you information about the Foundation, and the most important projects you should know for your software tooling or your projects. 
In particular I will introduce lightly the OSGi project and how it has been used to provide the powerful Eclipse 4 architecture (RCP). 
Then I will explain how the Eclipse Modeling project can help you to build your productivity tooling. Namely how you can create meta-models, editors for your models and code generators very easily using the EMF, XText, Xtend and Sirius projects.
So if you are a standalone application developper or a web developper you will discover powerful projects that will help you every day for your future developments.

Bio: Olivier Prouvost 
I am an Eclipse Expert located in France but often on the road... I use Eclipse for more than 12 years and I provide consulting and training on Eclipse RCP, Eclipse 4, Eclipse Modeling and all the different tooling around these technologies. 
