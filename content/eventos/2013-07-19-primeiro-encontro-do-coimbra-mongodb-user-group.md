---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 200
  event:
    location: Coimbra
    site:
      title: ''
      url: http://www.meetup.com/Coimbra-MongoDB-User-Group/events/126222892/
    date:
      start: 2013-07-19 00:00:00.000000000 +01:00
      finish: 2013-07-19 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Primeiro encontro do Coimbra Mongodb User Group
created: 1374244496
date: 2013-07-19
aliases:
- "/evento/200/"
- "/node/200/"
---

