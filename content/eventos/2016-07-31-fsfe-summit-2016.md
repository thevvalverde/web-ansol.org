---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 440
  event:
    location: Berlim
    site:
      title: ''
      url: https://www.fsfe.org/summit16
    date:
      start: 2016-09-02 00:00:00.000000000 +01:00
      finish: 2016-09-04 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: FSFE summit 2016
created: 1469920688
date: 2016-07-31
aliases:
- "/evento/440/"
- "/node/440/"
---

