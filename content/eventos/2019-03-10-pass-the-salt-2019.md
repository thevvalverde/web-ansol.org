---
categories:
- security
- libre software
metadata:
  mapa:
  - mapa_geom: !binary |-
      AQEAAAABAACATxcJQHDwGj7ITUlA
    mapa_geo_type: point
    mapa_lat: !ruby/object:BigDecimal 27:0.50607673419145e2
    mapa_lon: !ruby/object:BigDecimal 27:0.3136382102966e1
    mapa_left: !ruby/object:BigDecimal 27:0.3136382102966e1
    mapa_top: !ruby/object:BigDecimal 27:0.50607673419145e2
    mapa_right: !ruby/object:BigDecimal 27:0.3136382102966e1
    mapa_bottom: !ruby/object:BigDecimal 27:0.50607673419145e2
    mapa_geohash: u0fpzkgg5404bj2j
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 276
  - tags_tid: 316
  node_id: 658
  event:
    location: Polytech Lille, Lille, France
    site:
      title: Pass the Salt
      url: https://2019.pass-the-salt.org
    date:
      start: 2019-07-01 00:00:00.000000000 +01:00
      finish: 2019-07-03 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Pass the SALT 2019
created: 1552180511
date: 2019-03-10
aliases:
- "/evento/658/"
- "/node/658/"
---
<p>This conference hosts Security and Libre Talks (SALT), workshops, and networking opportunities for professionals in hacking Free Software, as well as in security. The themes of the conference also include Free Hardware, Open Formats, and research projects. The conference language is English and talks can either be long or short.</p>
