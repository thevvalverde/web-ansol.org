---
categories: []
metadata:
  mapa:
  - {}
  slide:
  - slide_value: 0
  node_id: 794
  event:
    location: Online
    site:
      title: ''
      url: https://ozgurkon.org/
    date:
      start: 2021-05-29 00:00:00.000000000 +01:00
      finish: 2021-05-30 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: ÖzgürKon 2021
created: 1619298060
date: 2021-04-24
aliases:
- "/evento/794/"
- "/node/794/"
---
<p>ÖzgürKon is an international online conference, organized by Özgür Yazılım Derneği. COVID-19 is created an event vacuum since people are no longer able to gather physicaly. ÖzgürKon is aimed to fill this void by bringing worldwide free software community together in virtual space and underline the importance of free software in the light of current developments.</p>
