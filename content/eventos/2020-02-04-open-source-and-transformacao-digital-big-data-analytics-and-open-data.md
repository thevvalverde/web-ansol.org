---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 730
  event:
    location: LNEC - Laboratório Nacional de Engenharia Civil, Lisboa
    site:
      title: ''
      url: https://www.eventbrite.com/e/open-source-transformacao-digital-big-data-analytics-open-data-tickets-91325778863
    date:
      start: 2020-02-20 00:00:00.000000000 +00:00
      finish: 2020-02-20 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: 'Open Source & Transformação Digital: Big Data Analytics & Open Data'
created: 1580808067
date: 2020-02-04
aliases:
- "/evento/730/"
- "/node/730/"
---

