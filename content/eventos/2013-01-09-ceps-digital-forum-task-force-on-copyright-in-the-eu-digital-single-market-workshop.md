---
categories: []
metadata:
  node_id: 118
  event:
    location: 
    site:
      title: ''
      url: http://www.ceps.be/taskforce/ceps-digital-forum-task-force-copyright-eu-digital-single-market
    date:
      start: 2013-01-16 00:00:00.000000000 +00:00
      finish: 2013-01-16 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: CEPS Digital Forum Task Force on Copyright in the EU Digital Single Market
  - Workshop
created: 1357734114
date: 2013-01-09
aliases:
- "/evento/118/"
- "/node/118/"
---

