---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 503
  event:
    location: 
    site:
      title: ''
      url: https://www.defectivebydesign.org/dayagainstdrm
    date:
      start: 2017-07-09 00:00:00.000000000 +01:00
      finish: 2017-07-09 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: "#DayagainstDRM - Dia Internacional contra o DRM 2017"
created: 1497474040
date: 2017-06-14
aliases:
- "/evento/503/"
- "/node/503/"
---
<img src="https://ansol.org/attachments/CadeadoByPaulaSimoes.jpg" alt="Fotografia por Paula Simoes Creative Commons BY" title="Fotografia por Paula Simoes Creative Commons BY">

DRM, Gestão de Restrições Digitais (Digital Restrictions Management) são
mecanismos que os fabricantes põem nos produtos e serviços para restringir
certas utilizações pelos consumidores. Não conseguir fazer uma cópia de um DVD;
não conseguir ler um livro digital em qualquer leitor; não conseguir extrair um
excerto de um livro digital ou filme para usar nas aulas; não conseguir
imprimir usando tinteiros de outras marcas; ser obrigado a estar online quando
se quer jogar um jogo, tudo isto são exemplos de como o DRM nos afecta no dia a
dia.

Estes mecanismos impedem frequentemente utilizações que seriam legais, e muito
comuns, de obras, ferramentas e dispositivos. Na prática, os cidadãos
encontram-se assim privados de muitos dos seus direitos, a não ser que
contornem o DRM.

Nos últimos 13 anos, o acto de contornar o DRM era crime em Portugal, punível
com até dois anos de prisão. No dia de 4 de Junho deste ano <a
href="http://ensinolivre.pt/?p=797">entrou finalmente em vigor uma alteração à
lei que permite aos consumidores neutralizar o DRM para fins legais</a>. Foi um
passo importante para devolver aos cidadãos os seus direitos, que está a ser
observado com interesse por outros países.

Apesar de, em Portugal, agora ser possível contornar o DRM nestas situações, o
seu impacto é limitado pelo facto de na maioria de outros países continuar a
ser proibido, incluindo todos os outros Estados-Membros da União Europeia. Além
disso, está planeada uma expansão da utilização e da protecção legal ao DRM.

No final do ano passado, a Comissão Europeia propôs uma alteração à Directiva
da Sociedade da Informação que está a ser discutida pelo Parlamento Europeu.
Vários deputados europeus submeteram emendas que permitiriam aos cidadãos
neutralizar o DRM para fins legais, à semelhança do que foi feito em Portugal.

AEL, D3 e FSF, entre outras organizações, juntam-se hoje a nós e milhares de
cidadãos, para espalhar a mensagem: “Não ao DRM”. Para além de marcar este dia,
há muito a fazer para garantir os direitos dos cidadãos. Por exemplo, é <a
href="http://www.europarl.europa.eu/meps/en/search.html?country=PT">importante
contactarmos os nossos representantes no Parlamento Europeu</a> para que eles
votem a favor das emendas que melhoram a situação legal.

Queres começar já a ajudar o movimento contra o DRM? Partilha este artigo!

<hr>

* ANSOL – Associação Nacional para o Software Livre [<a href="https://ansol.org/">https://ansol.org</a>]
* AEL – Associação Ensino Livre [<a href="http://ensinolivre.pt/">http://ensinolivre.pt</a>]
* Associação D3 – Defesa dos Direitos Digitais [<a href="https://direitosdigitais.pt/">https://direitosdigitais.pt/</a>]

Defective By Design – &nbsp;[<a href="http://dayagainstdrm.org">http://dayagainstdrm.org</a>]
