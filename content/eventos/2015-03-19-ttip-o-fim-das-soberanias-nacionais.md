---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 297
  event:
    location: Lisboa
    site:
      title: ''
      url: http://www.pan.com.pt/component/k2/item/496-2015-03-31-ttip.html
    date:
      start: 2015-03-31 18:30:00.000000000 +01:00
      finish: 2015-03-31 20:30:00.000000000 +01:00
    map: {}
layout: evento
title: TTIP - O fim das soberanias nacionais
created: 1426765657
date: 2015-03-19
aliases:
- "/evento/297/"
- "/node/297/"
---

