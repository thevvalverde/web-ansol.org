---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 492
  event:
    location: Auditório JJ Laginha, ISCTE, Lisboa
    site:
      title: 
      url: 
    date:
      start: 2017-03-28 18:00:00.000000000 +01:00
      finish: 2017-03-28 20:00:00.000000000 +01:00
    map: {}
layout: evento
title: Palestra de Presidente da FSFE em Lisboa
created: 1489242808
date: 2017-03-11
aliases:
- "/FSFE-palestra/"
- "/evento/492/"
- "/node/492/"
---
<p><a href="https://fsfe.org/about/kirschner/kirschner.en.html">Matthias Kirschner</a>, presidente da <a href="https://fsfe.org/index.pt.html">Free Software Foundation Europe</a>, estará em Lisboa para uma palestra entitulada <strong>The long way to empower people to control technology</strong>.</p><p><img src="https://www.gnome.org/wp-content/uploads/2015/08/Matthias-Kirschner-present-Keynote.jpg" alt="Matthias Kirschner, presidente da FSFE" title="Matthias Kirschner, presidente da FSFE" style="display: block; margin-left: auto; margin-right: auto;" height="469" width="704"></p><p><strong>The long way to empower people to control technology </strong></p><p>Software is deeply involved in all aspects of our lives. It is important that this technology empowers rather than restricts us. Free Software gives everybody the rights to use, understand, adapt and share software.&nbsp; These rights help support other fundamental rights like freedom of speech, freedom of press and privacy.<br class="gmail_msg"><br class="gmail_msg">The Free Software Foundation Europe is a charity that empowers users to control technology by:</p><ul><li>Helping individuals and organisations to understand how Free Software contributes to freedom, transparency, and self-determination.</li><li>Enhancing users' rights by abolishing barriers to Free Software adoption.</li><li>Encouraging people to use and develop Free Software.</li><li>Providing resources to enable everyone to further promote Free Software in Europe.</li></ul><p>In this talk Matthias will talk about how he joined the FSFE, highlight the importance of Free Software for society, give some examples of the FSFE's work from the past 15 years, and show how you as individual or organisation can work with us the FSFE and its local associate ANSOL for software freedom.</p><div class="F3hlO">This talk, presented in English, is followed by a Q&amp;A session you'll be invited to participate in, and in there we will cover typical misunderstandings surounding Free Software.</div><div class="F3hlO">&nbsp;</div><div class="F3hlO">This event is kindly supported by <a href="http://moss.dcti.iscte.pt/">MOSS</a>, the Open Source Masters programme in ISCTE, and <a href="https://iscte.acm.org/">ISCTE's ACM Student Chapter</a>.</div><div class="F3hlO">&nbsp;</div><div class="F3hlO"><hr>Depois do evento, haverá um jantar-convívio para os interessados, mediante inscrição prévia via e-mail para <a href="mailto:marcos.marado@ansol.org">marcos.marado@ansol.org</a> .</div>
