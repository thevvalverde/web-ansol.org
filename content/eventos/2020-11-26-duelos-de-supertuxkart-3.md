---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 764
  event:
    location: https://meet.ubcasts.org/STKIII
    site:
      title: 
      url: 
    date:
      start: 2020-11-26 21:00:00.000000000 +00:00
      finish: 2020-11-26 21:00:00.000000000 +00:00
    map: {}
layout: evento
title: Duelos de SuperTuxKart 3
created: 1606396729
date: 2020-11-26
aliases:
- "/evento/764/"
- "/node/764/"
---
<p>Esta quinta-feira, a comunidade Ubuntu Portugal reúne-se como faz sempre em uma quinta-feira de cada mês. <br>Venham daí jogar e mostrar-nos as vossas fantásticas habilidades de gamer, na quinta feira dia 26 a partir das <strong>21h locais de Portugal continental</strong>.</p><p>Vamos guiar-vos sobre na instalação do SuperTuxKart (idealmente em Ubuntu ;) e depois vamos todos jogar juntos no nosso servidor.</p><p>A conversa que antecede o jogo e que durante o jogo irá decorrer no Jitsi (só precisam de um browser/navegador para web) no endereço:<br>https://meet.ubcasts.org/STKIII </p><p>Juntem-se a nós!</p><p>Nota 1: o SuperTuxKart é um jogo multi-plataforma, podendo ser jogado em Ubuntu, qualquer outras distribuição de GNU/Linux, MacOS, Windows e até Android.<br>Nota 2: o hostname do servidor de SuperTuxKart é: stk.ubcasts.net</p>
