---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 696
  event:
    location: 
    site:
      title: ''
      url: https://www.fpc.pt/event/oficina-a-exposicao-ganhou-vida-com-o-scratch/
    date:
      start: 2019-09-21 00:00:00.000000000 +01:00
      finish: 2019-09-21 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: 'SFD: Oficina “A exposição ganhou vida com o Scratch”'
created: 1568839523
date: 2019-09-18
aliases:
- "/evento/696/"
- "/node/696/"
---
<p>No dia em que se celebra o Dia do Software Livre, o Museu das Comunicações dinamiza uma atividade utilizando o software de programação Scratch Jr..</p>
