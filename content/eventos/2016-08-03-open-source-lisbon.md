---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 441
  event:
    location: Lisboa
    site:
      title: ''
      url: http://www.opensourcelisbon.com/
    date:
      start: 2016-09-29 00:00:00.000000000 +01:00
      finish: 2016-09-29 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Open Source Lisbon
created: 1470229747
date: 2016-08-03
aliases:
- "/evento/441/"
- "/node/441/"
---
<p class="ProfileHeaderCard-bio u-dir" dir="ltr">Open Source Lisbon is a one day event for business professionals &amp; tech enthusiasts, with interest in everything related to Open Source.</p>
