---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 248
  event:
    location: 
    site:
      title: ''
      url: https://www.nao-ao-ttip.pt/coloquio-internacional-oportunidades-e-riscos-do-ttip/
    date:
      start: 2014-11-21 15:00:00.000000000 +00:00
      finish: 2014-11-21 18:30:00.000000000 +00:00
    map: {}
layout: evento
title: 'Colóquio Internacional: Oportunidades e Riscos do TTIP'
created: 1416578345
date: 2014-11-21
aliases:
- "/evento/248/"
- "/node/248/"
---

