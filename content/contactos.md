---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 35
layout: page
title: Contactos
created: 1334481282
date: 2012-04-15
aliases:
- "/contacto/"
- "/contato/"
- "/node/35/"
- "/page/35/"
---

## Correio Electrónico

* Geral: [contacto@ansol.org](mailto:contacto@ansol.org)
* Sobre o site: [webmaster@ansol.org](mailto:webmaster@ansol.org)
* Discussão e participação: [listas de correio](https://membros.ansol.org/mailman/listinfo)
* Página de contacto dos [órgãos sociais em exercício](/orgaos-sociais)

## Redes sociais

* Twitter: [@ANSOL](https://twitter.com/ANSOL)
* Mastodon: [@ansol@floss.social](https://floss.social/@ansol)
* Peertube: [@ansol@viste.pt](https://viste.pt/c/ansol/)

Temos uma sala Matrix aberta ao público: <https://matrix.to/#/#geral:ansol.org>

## Informações adicionais

* IBAN: **PT50&nbsp;0035&nbsp;2178&nbsp;00027478430&nbsp;14**
* NIPC: **513661174**
* Número de Registo de Transparência da UE: [736822225106-05](https://ec.europa.eu/transparencyregister/public/consultation/displaylobbyist.do?isListLobbyistView=true&id=736822225106-05)
