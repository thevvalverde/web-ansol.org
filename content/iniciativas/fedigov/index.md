---
layout: article
title: Fedigov
date: 2022-12-19
featured: true
summary: |
  As redes sociais têm um papel cada vez mais importante nas instituições
  públicas. Permitem que haja um canal de comunicação directo com o público e que
  informação relevante seja disseminada em tempo útil.

  Ao se juntarem ao Fediverso, as instituições públicas passam a contribuir
  para uma rede de comunicação independente e soberana, livre da influência e
  controlo das megacorporações que gerem as redes sociais privadas. O público
  deixa de ser obrigado a ceder a sua informação pessoal a estas entidades para
  poder acompanhar as comunicações das instituições públicas.
---

As redes sociais têm um papel cada vez mais importante nas instituições
públicas. Permitem que haja um canal de comunicação directo com o público e que
informação relevante seja disseminada em tempo útil.

Ao se juntarem ao Fediverso, as instituições públicas passam a contribuir
para uma rede de comunicação independente e soberana, livre da influência e
controlo das megacorporações que gerem as redes sociais privadas. O público
deixa de ser obrigado a ceder a sua informação pessoal a estas entidades para
poder acompanhar as comunicações das instituições públicas.

Com a campanha FediGov, a ANSOL quer aumentar a sensibilização para este
problema e oferecer tanto ao público como às instituições oportunidades
concretas. O público é incentivado a contactar as instituições públicas para
pedir que divulguem as suas comunicações no Fediverso. Às instituições públicas
é oferecida informação sobre redes sociais descentralizadas baseadas em
Software Livre.

O website da campanha é [https://fedigov.pt][fedigov].

[fedigov]: https://fedigov.pt
