---
title: Documentação e Transparência
---

- [Estatutos]({{< ref "estatutos" >}})
- [Regulamento Interno]({{< ref "regulamento-interno" >}})
- [Reuniões da Direcção](https://membros.ansol.org/node/22)
- [Assembleias Gerais](https://membros.ansol.org/AG), apenas visíveis a membros da ANSOL


## Protocolos

A ANSOL assinou, em agosto de 2022, protocolos de parceria com os Agrupamentos de Escolas
de Santa Maria da Feira e Augusto Cabrita:

- [Protocolo de parceria com Agrupamento de Escolas de Santa Maria da Feira][protocolo-feira]
- [Protocolo de parceria com Agrupamento de Escolas Augusto Cabrita][protocolo-barreiro]

[protocolo-feira]: /protocolos/smfeira.pdf
[protocolo-barreiro]: /protocolos/barreiro.pdf

## Reuniões com partidos políticos, deputados e entidades governamentais

- Fevereiro de 2023:
  - Eurodeputada Maria da Graça Carvalho, PSD | Parlamento Europeu - Bruxelas | ChatControl e o Direito de instalar qualquer software em qualquer dispositivo
  - Eurodeputado Francisco Guerreiro, Independente | Parlamento Europeu - Bruxelas | ChatControl e o Direito de instalar qualquer software em qualquer dispositivo
  - Assistente da Eurodeputada Sandra Pereira, PCP | Parlamento Europeu - Bruxelas | ChatControl e o Direito de instalar qualquer software em qualquer dispositivo
  - Eurodeputado José Gusmão, BE | Parlamento Europeu - Bruxelas | ChatControl
  - Assistentes dos Eurodeputados Carlos Zorrinho e Isabel Santos, PS | Parlamento Europeu - Bruxelas | ChatControl

## Relatórios anuais

- [Plano de actividades para 2022](/relatorios/2022-plano-de-actividades.pdf), apresentado em Assembleia Geral a 5 de março de 2022;
- [Relatório de Actividade e Contas de 2021](/relatorios/2021-relatorio-actividade-e-contas.pdf), apresentado em Assembleia Geral a 5 de março de 2022;

- Plano de actividades para 2021: não foi redigido, porque a Assembleia Geral foi adiada para dezembro;
- [Relatório de Actividade e Contas de 2020](/relatorios/2020-relatorio-actividade-e-contas.pdf), apresentado em Assembleia Geral a 18 de dezembro de 2021;

- Plano de actividades para 2020: em falta;
- [Relatório de Actividade e Contas de 2019](/relatorios/2019-relatorio-actividade-e-contas.pdf), apresentado em Assembleia Geral a 29 de fevereiro de 2020;

- [Plano de actividades para 2019](/relatorios/2019-plano-de-actividades-e-orcamento.pdf), apresentado em Assembleia Geral a 16 de março de 2019;
- [Relatório de Actividade e Contas de 2018](/relatorios/2018-relatorio-actividade-e-contas.pdf): apresentado em Assembleia Geral a 16 de março de 2019;

- [Plano de actividades para 2018](/relatorios/2018-plano-de-actividades-e-orcamento.pdf), apresentado em Assembleia Geral a 27 de janeiro de 2018;
- [Relatório de Actividade e Contas de 2017](/relatorios/2017-relatorio-actividade-e-contas.pdf): apresentado em Assembleia Geral a 27 de janeiro de 2018;

- [Plano de actividades para 2017](/relatorios/2017-plano-de-actividades-e-orcamento.pdf): apresentado em Assembleia Geral a 12 de março de 2017;
- Relatório de Actividade e Contas de 2016: em falta;

- Plano de actividades para 2016: em falta;
- [Relatório de Actividade e Contas de 2015](/relatorios/2015-relatorio-actividade-e-contas.pdf), apresentado em Assembleia Geral a 17 de setembro de 2016;

- Plano de actividades para 2015: em falta;
