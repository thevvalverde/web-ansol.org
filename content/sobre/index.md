---
categories: []
metadata:
  slide:
  - slide_value: 1
  node_id: 1
layout: page
title: Sobre a ANSOL
created: 1332695512
date: 2012-03-25
aliases:
- "/node/1/"
- "/page/1/"
---
<div style="font-size: 1.2em; line-height: 1.5em">

A <strong>ANSOL - Associação Nacional para o Software Livre</strong> é uma
associação portuguesa sem fins lucrativos que tem como fim a divulgação,
promoção, desenvolvimento, investigação e estudo da Informática Livre e das
suas repercussões sociais, políticas, filosóficas, culturais, técnicas e
científicas.


<div style="display: flex; align-items: center; margin: 2em 0;">
  <img src="ep.svg" style="width: 60px; margin-right: 20px;" alt="">

  <p style="margin-bottom: 0px">
    Participamos em grupos e comissões parlamentares tanto a nível nacional como
    europeu, visando o esclarecimento e defesa da utilização de software livre em
    vários contextos.
  </p>
</div>

<div style="display: flex; align-items: center; margin: 2em 0;">
  <img src="community.svg" style="width: 60px; margin-right: 20px;" alt="">

  <p style="margin-bottom: 0px">
    Fazemos parte da organização de eventos de divulgação de Software Livre,
    como a Ubucon Europe ou a Festa do Software Livre. Celebramos todos os anos
    datas como o <a href="https://www.softwarefreedomday.org/">Software Freedom Day</a>
    e o <a href="https://www.documentfreedom.org/">Document Freedom Day</a>.
  </p>
</div>

<div style="display: flex; align-items: center; margin: 2em 0;">
  <img src="fsfe.svg" style="width: 60px; margin-right: 20px;" alt="">

  <p style="margin-bottom: 0px">
    Somos uma organização associada da
    <a href="https://fsfe.org">Free Software Foundation Europe</a>,
    colaborando em várias iniciativas como a <a href="/publiccode">Dinheiro
    Público? Código Público</a>.
  </p>
</div>

<div style="display: flex; align-items: center; margin: 2em 0; gap: 20px;">
  <img src="volunteer.svg" style="width: 60px;" alt="">

  <p style="margin-bottom: 0px">
    Vivemos de voluntariado, e estamos sempre à procura de
    pessoas preocupadas com a liberdade tecnológica. Se estes temas te
    interessam, inscreve-te!
  </p>
</div>

<p style="display: flex; justify-content: center; line-height: 3em; margin-bottom: 3em;">
  <a href="{{< ref "/inscricao" >}}" class='cta' style="margin-left: 0px;">Junta-te a nós</a>
</p>

<div style="display: flex; align-items: center; margin: 2em 0; gap: 20px;">
  <a href="{{< ref "/orgaos-sociais" >}}" style="display: block;">
    <img src="orgaos-sociais.svg" style="width: 60px; display: block;" alt="">
  </a>

  <p style="margin-bottom: 0px">
    <a href="{{< ref "/orgaos-sociais" >}}"><strong>Órgãos sociais</strong></a>:
    Conhece as pessoas que compõe a nossa equipa.
  </p>
</div>

<div style="display: flex; align-items: center; margin: 2em 0; gap: 20px;">
  <a href="{{< ref "/documentacao" >}}" style="display: block;">
    <img src="documentacao.svg" style="width: 60px; display: block;" alt="">
  </a>

  <p style="margin-bottom: 0px">
    <a href="{{< ref "/documentacao" >}}"><strong>Documentação e Transparência</strong></a>:
    Consulta os nossos estatutos e regulamentos, assim como os relatórios de actividade e de contas.
  </p>
</div>


<div style="display: flex; align-items: center; margin: 2em 0; gap: 20px;">
  <a href="{{< ref "/contactos" >}}" style="display: block;">
    <img src="contact.svg" style="width: 60px; display: block;" alt="">
  </a>

  <p style="margin-bottom: 0px">
    <a href="{{< ref "/contactos" >}}"><strong>Contactos</strong></a>:
    Estamos disponíveis para falar. Temos também canais de comunicação
    abertos a toda a comunidade.
  </p>
</div>
</div>

<!--
<a href="/filosofia/softwarelivre.pt.html">O que é o Software Livre ?</a>
<a href="/recursos">Recursos disponíveis para projectos/eventos de Software Livre</a>
-->
