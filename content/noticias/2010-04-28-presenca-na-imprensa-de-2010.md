---
categories:
- imprensa
- '2010'
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 19
  - tags_tid: 29
  node_id: 156
layout: page
title: Presença na Imprensa de 2010
created: 1272455999
date: 2010-04-28
aliases:
- "/imprensa/2010/"
- "/node/156/"
- "/page/156/"
---
<ul><li><p class="line862">2010-11-15:&nbsp;<a href="http://tek.sapo.pt/noticias/negocios/software_livre_permite_poupar_80_milhoes_diz_1106346.html" class="http">Software livre permite poupar 80 milhões, diz ESOP</a></p></li><li><p class="line862">2010-09-29:&nbsp;<a href="http://tek.sapo.pt/noticias/negocios/ansol_volta_a_criticar_despesismo_da_ap_nas_c_1095287.html" class="http">ANSOL volta a criticar despesismo da AP nas compras de software</a></p></li><li><p class="line862">2010-09-29:&nbsp;<a href="http://aeiou.exameinformatica.pt/ansol-diz-que-o-estado-desperdica-121-milhoes-a-comprar-software-a-microsoft=f1007325" class="http">ANSOL diz que o Estado desperdiça 121 milhões a comprar software à Microsoft</a></p></li><li><p class="line862">2010-04-22:&nbsp;<a href="http://tek.sapo.pt/noticias/negocios/governo_avanca_com_criacao_de_tribunal_para_p_1060172.html" class="http">Governo avança com criação de Tribunal para Propriedade Intelectual</a></p></li></ul>
