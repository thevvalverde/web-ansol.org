---
categories:
- eu
- financiamento
- horizonte 2020
- software livre
- dados abertos
- normas abertas
- imprensa
metadata:
  tags:
  - tags_tid: 94
  - tags_tid: 188
  - tags_tid: 189
  - tags_tid: 41
  - tags_tid: 190
  - tags_tid: 87
  - tags_tid: 19
  node_id: 477
layout: article
title: ANSOL participa na avaliação do programa Horizonte 2020
created: 1484524291
date: 2017-01-15
aliases:
- "/Horizonte2020/"
- "/article/477/"
- "/node/477/"
---
<p>O Horizonte 2020 – Programa-Quadro Comunitário de Investigação &amp; Inovação, com um orçamento global superior a 77 mil milhões de euros para o período 2014-2020, é o maior instrumento da Comunidade Europeia especificamente orientado para o apoio à investigação, através do cofinanciamento de projetos de investigação, inovação e demonstração.<br><br>Uma avaliação intercalar a este programa está a decorrer durante o ano de 2017, e, para esse efeito, a Comissão Europeia ouviu a avaliação que é feita do programa por partes interessadas.<br><br>A ANSOL acredita que a informática tem um papel fulcral no desenvolvimento, na investigação e na inovação. Acreditamos, também, que a melhor forma de potenciar o investimento feito nestas áreas, para que os objectos deste programa sejam cumpridos, é que o resultado da investigação por ele financiada ocorra num ambiente aberto e de colaboração: que os dados usados e gerados sejam tornados abertos, que se garanta o acesso a eles através do uso de normas abertas, e, é claro, que o software produzido no decorrer destes programas seja disponibilizado como Software Livre, para que toda a sociedade possa beneficiar e usufruir dele.<br><br>Assim a ANSOL enviou o seu contributo, em que se foca essencialmente nas seguintes recomendações:<br><br></p><ol><li>O uso de Normas Abertas é necessário no uso de partilha de conhecimento, incluindo na divulgação de publicações científicas e no arquivo de dados, artigos e software usados na investigação;</li><li>O software desenvolvido com financiamento público deve ser publicado como Software Livre, e arquivado em repositórios públicos de software, garantindo a sua disponibilização mesmo após o termo dos períodos de financiamento;</li><li>Os repositórios de dados e software devem usar Software Livre, garantindo o acesso não discriminatório aos seus conteúdos, e a longevidade dos sistemas.</li></ol><p><br>O contributo completo da ANSOL pode ser lido <a href="/attachments/ANSOL-Horizonte2020.pdf">aqui</a>.</p>
