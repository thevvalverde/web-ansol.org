---
categories:
- newsletter
layout: article
title: Newsletter 2022 - 1º trimestre
date: 2022-04-20
---

No 1º trimestre de 2022 a ANSOL focou-se especialmente nas legislativas de 2022
e celebrou algumas tradições próprias desta altura do ano.

<!--more-->

Celebrámos o [Dia do Domínio Público][ddp] em conjunto com a Biblioteca
Nacional de Portugal e a Wikimedia Portugal, que permitiu a publicação da
listagem das obras de autores portugueses que entraram este ano em domínio
público. A procura dos autores e obras tem sido um trabalho que a ANSOL começou
por fazer manualmente, mas que tem vindo a automatizar nos últimos anos.

No contexto das eleições legislativas portuguesas de 2022, começámos o ano por
elaborar uma [lista de ideias para a próxima legislatura][ideias], e fizemos
uma [análise dos programas eleitorais][programas] apresentados pelos partidos
no que toca a Software Livre e Direitos Digitais.

- [Entrevista no programa "Mundo Digital" da TSF][tsf]
- [Cobertura no SAPO TEK][tek]
- [Cobertura na Revista PCGuia][pcguia]

Houve uma Assembleia Geral Eleitoral onde foram eleitos os órgãos sociais da
associação e alterados os estatutos e regulamento interno para permitir a
convocatória por correio electrónico para as próximas assembleias gerais.

Celebrámos também o o [dia de "Eu adoro Software Livre"][ilovefs] ao divulgar
vários projectos de software livre nas redes sociais.

Para terminar o trimestre, organizámos uma conversa informal no [Dia da
Liberdade Documental][dfd] à volta da Lei das Normas Abertas e do Regulamento
Nacional de Interoperabilidade Digital.

Reactivamos a nossa conta de Mastodon! Segue-nos em
<https://floss.social/@ansol>

Lançamos o novo repositório de código da ansol: <https://git.ansol.org/ansol>

Agradecemos publicamente aos novos **sete** sócios que aderiram à associação
este trimestre, e com quem estamos ansiosos por colaborar.


[ddp]: https://ansol.org/noticias/2022-01-01-dia-do-dominio-publico/
[ideias]: https://ansol.org/noticias/2022-01-03-dez-ideias-para-a-proxima-legislatura/
[programas]: https://ansol.org/noticias/2022-01-19-software-livre-nos-programas-eleitorais-2022/
[tsf]: https://www.tsf.pt/programa/mundo-digital/emissao/a-importancia-do-software-livre-nas-proximas-eleicoes-14527370.html
[tek]: https://tek.sapo.pt/noticias/negocios/artigos/o-que-dizem-os-programas-dos-partidos-sobre-software-livre-e-direitos-digitais
[pcguia]: https://www.pcguia.pt/2022/01/software-open-source-nos-programas-dos-partidos-para-as-legislativas-2022-ansol-elogia-ideias-da-il-e-do-pan/
[ilovefs]: https://ansol.org/noticias/2022-02-14-eu-adoro-software-livre-2022/
[dfd]: https://ansol.org/eventos/2022-03-30-document-freedom-day/
[ag]: https://ansol.org/eventos/2022-03-05-assembleia-geral-2022-eleitoral/
