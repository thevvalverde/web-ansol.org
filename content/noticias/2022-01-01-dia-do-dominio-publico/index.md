---
categories:
- domínio público
- portugal
- autores
- direito de autor
- copyright
layout: article
aliases:
- "/dominio-publico-2022/"
title: Feliz Dia do Domínio Público 2022
date: 2022-01-01
---

No dia 1 de Janeiro, em Portugal, entram em domínio público as obras daqueles autores que morreram há mais de 70 anos. Continuando uma [tradição anual](https://ansol.org/eventos/2020-12-01-dia-do-dominio-publico-2021/), a ANSOL celebra o dia de hoje, este ano juntamente com a Biblioteca Nacional de Portugal e a Wikimedia Portugal.

A Biblioteca Nacional de Portugal publicou no seu site a [lista dos autores Portugueses cujas obras entram hoje em domínio público](http://www.bnportugal.gov.pt/index.php?option=com_content&view=article&id=1659%3Anoticia-dia-do-dominio-publico-1-janeiro-2022&catid=173%3A2022&Itemid=1660&lang=pt).

A Wikipedia também publicou uma [lista de autores](https://pt.wikipedia.org/wiki/Lista_de_autores_portugueses_que_entram_em_dom%C3%ADnio_p%C3%BAblico_em_2022), obtida através da Wikidata, e um artigo no seu site que pode ser [lido aqui](https://blog.wikimedia.pt/2022/01/01/dia-do-dominio-publico-2022/).

---

*Nota:* A imagem deste artigo representa os Paços de Sintra, e é uma ilustração de Rainha D. Amélia, hoje em Domínio Público.
