---
categories:
- imprensa
- '2005'
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 19
  - tags_tid: 24
  node_id: 151
layout: page
title: Presença na Imprensa de 2005
created: 1114689042
date: 2005-04-28
---
<ul><li><p class="line891"><em><a href="http://tek.sapo.pt/extras/site_do_dia/programas_eleitorais_ponto_a_ponto_885943.html" class="http">Programas Eleitorais ponto a ponto</a></em>, Publicado por Casa dos Bits às 08.00h no dia 09 de Fevereiro de 2005</p></li><li><p class="line891"><em><a href="http://tek.sapo.pt/noticias/computadores/ansol_volta_a_organizar_manifestacao_contra_p_872142.html" class="http">Ansol volta a organizar manifestação contra patentes de software</a></em>, Publicado por Casa dos Bits às 09.57h no dia 20 de Junho de 2005</p></li><li><p class="line891"><em><a href="http://semanainformatica.xl.pt/751/act/100.shtml" class="http">Patentes de software rejeitadas</a></em>, Claudia Sargento, Semana Informática nº 751 de 15 a 21 de Julho de 2005</p></li></ul>
