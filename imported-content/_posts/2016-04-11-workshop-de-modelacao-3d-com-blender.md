---
categories: []
metadata:
  event_location:
  - event_location_value: Águeda
  event_site:
  - event_site_url: http://all.cm-agueda.pt/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2016-04-22 23:00:00.000000000 +01:00
    event_start_value2: 2016-04-22 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 413
layout: evento
title: Workshop de Modelação 3D com Blender
created: 1460389342
date: 2016-04-11
---
<h3>Workshop Modelação 3D com Blender</h3><h4>23 de abril<br>Início: 14h30<br>Fim: 17h30</h4><p>Inscrições gratuitas | Lugares limitados<br><br>É recomendado aos participantes que tragam computador portátil e rato.</p><p><span><strong>local de realização</strong>: Incubadora de Empresas de Águeda<br>Rua Luís de Camões, nº 64<br>Águeda</span><br> <span><strong>email</strong>: <a href="mailto:all@cm-agueda.pt">all@cm-agueda.pt</a></span><br> <span><strong>duração do evento</strong>: 3 horas</span><br><br></p><h3>período de pré-inscrição</h3><p>entre os dias <strong>07</strong> e <strong>22</strong> de <strong>Abril</strong> de <strong>2016</strong><br>das <strong>01:00</strong> às <strong>23:59</strong></p><p>&nbsp;</p><p><strong>Inscrição em: http://all.cm-agueda.pt/</strong></p>
