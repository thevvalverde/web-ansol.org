---
categories: []
metadata:
  event_location:
  - event_location_value: Anfiteatro B1 do Departamento de Engenharia Informática
      da Universidade de Coimbra
  event_site:
  - event_site_url: http://jugevents.org/jugevents/event/44999
    event_site_title: http://jugevents.org/jugevents/event/44999
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2012-04-17 23:00:00.000000000 +01:00
    event_start_value2: 2012-04-17 23:00:00.000000000 +01:00
  node_id: 31
layout: evento
title: 8º encontro do  PT.JUG
created: 1333706744
date: 2012-04-06
---
<p>Est&atilde;o abertas as inscri&ccedil;&otilde;es para o 8&ordm; encontro do PT.JUG que se vai realizar j&aacute; no pr&oacute;ximo dia 18 de Abril no anfiteatro B1 do Departamento de Engenharia Inform&aacute;tica da Universidade de Coimbra.</p>
<p>Toda a informa&ccedil;&atilde;o sobre o evento est&aacute; dispon&iacute;vel no grupo PT.JUG em <a href="https://groups.google.com/d/topic/ptjug-geral/f6KYo5RE0jU/discussion">https://groups.google.com/d/topic/ptjug-geral/f6KYo5RE0jU/discussion</a></p>
<p>&nbsp;PT.JUG (Portuguese Java User Group) &eacute; o grupo portugu&ecirc;s de utilizadores Java. Foi criado com o intuito de divulgar e fomentar discuss&otilde;es sobre a linguagem de programa&ccedil;&atilde;o e plataforma Java e existe desde 2007.</p>
<p>A entrada do evento &eacute; completamente gratuita, no entanto &eacute; necess&aacute;ria a inscri&ccedil;&atilde;o no grupo <a href="http://groups.google.com/group/ptjug-geral">http://groups.google.com/group/ptjug-geral</a> e o registo no evento em <a href="http://jugevents.org/jugevents/event/44999">http://jugevents.org/jugevents/event/44999</a></p>
