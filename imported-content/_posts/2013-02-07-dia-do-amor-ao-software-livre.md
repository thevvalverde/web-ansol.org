---
categories: []
metadata:
  event_location:
  - event_location_value: Em todo o lado
  event_site:
  - event_site_url: http://fsfe.org/campaigns/ilovefs/ilovefs.pt.html
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2013-02-14 00:00:00.000000000 +00:00
    event_start_value2: 2013-02-14 00:00:00.000000000 +00:00
  node_id: 129
layout: evento
title: Dia do Amor ao Software Livre
created: 1360197747
date: 2013-02-07
---
<p>O Dia de S&atilde;o Valentim &eacute; tradicionalmente um dia para mostrar e celebrar o amor. Porque n&atilde;o aproveitar esta oportunidade para mostrar o seu amor pelo Software Livre este ano?</p>
