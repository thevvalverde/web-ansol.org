---
categories: []
metadata:
  event_location:
  - event_location_value: Online
  event_site:
  - event_site_url: https://blog.documentfoundation.org/blog/2021/04/13/dates-for-virtual-libocon/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2021-09-22 23:00:00.000000000 +01:00
    event_start_value2: 2021-09-24 23:00:00.000000000 +01:00
  mapa:
  - {}
  slide:
  - slide_value: 0
  node_id: 784
layout: evento
title: LibreOffice Virtual Conference
created: 1618316522
date: 2021-04-13
---

