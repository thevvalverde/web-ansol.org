---
categories: []
metadata:
  event_location:
  - event_location_value: Sede do IINFACTS/Museu de Penafiel
  event_site:
  - event_site_url: http://ciencias.iscsn.cespu.pt/eventos/2012/o-crime-no-sec-xxi.html
    event_site_title: http://ciencias.iscsn.cespu.pt/eventos/2012/o-crime-no-sec-xxi.html
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2012-04-20 07:00:00.000000000 +01:00
    event_start_value2: 2012-04-21 19:00:00.000000000 +01:00
  node_id: 15
layout: evento
title: O Crime no séc. XXI
created: 1333179536
date: 2012-03-31
---
<p>As VI Jornadas de Ci&ecirc;ncias do ISCS-N, entituladas &quot;O Crime no s&eacute;c. XXI&quot;, s&atilde;o compostas por tr&ecirc;s sess&otilde;es, a terceira intitulada &quot;O crime espreita novos paradigmas&quot;. Nesse &acirc;mbito a ANSOL ir&aacute; fazer uma apresenta&ccedil;&atilde;o sobre o tema &quot;Censura na Web?&quot;.</p>
