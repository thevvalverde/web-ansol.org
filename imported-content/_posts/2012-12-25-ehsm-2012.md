---
categories: []
metadata:
  event_site:
  - event_site_url: http://ehsm.eu/index.html
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2012-12-28 00:00:00.000000000 +00:00
    event_start_value2: 2012-12-30 00:00:00.000000000 +00:00
  node_id: 115
layout: evento
title: EHSM 2012
created: 1356475634
date: 2012-12-25
---
<p>Exceptionally Hard &amp; Soft Meeting 2012 - um evento sobre software e hardware livres na comunidade DIY, em Berlin, com live streaming do evento, em Ingl&ecirc;s, dispon&iacute;vel gratuitamente.</p>
