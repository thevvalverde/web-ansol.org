---
categories:
- braga
- blockchain
- universidade do minho
metadata:
  event_location:
  - event_location_value: Universidade do Minho, Gualtar - Braga - Sala A2 - Dep.
      Informática
  event_site:
  - event_site_url: https://www.meetup.com/Braga-Bitcoin-Ethereum-Blockchain/events/247712900/
    event_site_title: Hacking on the first Blockchain Technology Workshop
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2018-03-03 10:00:00.000000000 +00:00
    event_start_value2: 2018-03-03 23:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 208
  - tags_tid: 209
  - tags_tid: 210
  node_id: 547
layout: evento
title: Hacking on the first Blockchain Technology Workshop
created: 1519935082
date: 2018-03-01
---
<p>We're very excited to announce our first full-day Blockchain technology workshop!<br><br>Date: Saturday 3rd of March on the A2 Auditorium of the University of Minho at Gualtar Campus.<br><br>Morning Talks<br><br>10:00 - 11:00 Blockchains, the reason behind things<br><br>Speaker: Ali Shoker from HASLab, INESC TEC &amp; University of Minho<br><br>Abstract: Many people think that Bitcoin and cryptocurrencies are just an alternative to digital Fiat money, and a bubble released by Satoshi Nakamoto. In this talk, we emphasize on the importance of blockchains as the backbone of cryptocurrencies in two ways. The first is being a nontrivial solution to an open research question: how to achieve a globally scalable total order? The second is that blockchains are the infrastructure for many use-cases, beyond cryptocurrencies. The talk explains the reason behind things and touches upon how things are done in blockchains. The difficulty will be incremental, but suitable for a general audience with fair technical (but not necessarily programming) knowledge.<br><br>Website: <a href="http://www.alishoker.com" target="__blank" title="http://www.alishoker.com" class="link">http://www.alishoker.com</a> | Twitter: @ ashokerCS<br><br>11:00 - 11:20 - Coffee Break and Networking<br><br>11:20 - 12:20 Programming Smart Contracts with Solidity on Ethereum<br><br>Speaker: Emanuel Mota from Yari Labs<br><br>Abstract: We will start to describe what are smart contracts and how can we program and deploy a smart contract. We'll get a basic understanding of how the Ethereum virtual machine executes contracts and we'll cover the basics of Solidity Programming language with examples. The goal of this talk is to give enough context and preparation for the practical workshop in the afternoon. The difficulty will be incremental, but suitable for a general audience having programming knowledge will help but is not required.<br><br>Website: <a href="http://www.yarilabs.com" target="__blank" title="http://www.yarilabs.com" class="link">http://www.yarilabs.com</a> - Twitter: @emota7 || @yarilabs<br><br>12h30 - 14h00 Lunch Break<br><br>14h - 17h - Practical Workshop (with a coffee break in between)<br><br>You need to bring your laptop or come with someone that has a laptop, although participants will be organized into groups preferably having someone with programming experience in each group.<br><br>- First, we'll code some initial exercises to play with solidity.<br>- Then we'll proceed to implement a project that uses a smart contract and stores content on the blockchain.<br>- The groups will present their work.<br><br>We'll have coaches to assist the groups.<br><br>The talks in the morning will be held in English, in the afternoon during the hack session there will be a mix of Portuguese and English :)<br>Don't miss this great opportunity to get your hands dirty with blockchain programming!<br><br>IMPORTANT:<br>There will be a registration desk where will give participants a sticker with their name. For security reasons the University services need to know the number of participants until next Tuesday (27 of February) so the RSVP's will be closed after that day.</p>
