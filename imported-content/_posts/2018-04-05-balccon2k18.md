---
categories: []
metadata:
  event_location:
  - event_location_value: Novi Sad, Vojvodina, Serbia, Europe, Earth, Milky Way
  event_site:
  - event_site_url: https://2k18.balccon.org/index.php?title=Main_Page
    event_site_title: BalCCon2k18
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2018-09-13 23:00:00.000000000 +01:00
    event_start_value2: 2018-09-15 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 598
layout: evento
title: BalCCon2k18
created: 1522935660
date: 2018-04-05
---
<center><h2><span id="Balkan_Computer_Congress_-_BalCCon2k18" class="mw-headline"></span>Balkan Computer Congress - BalCCon2k18</h2><div class="center"><div class="floatnone"><a href="https://2k18.balccon.org/index.php?title=File:BalCCon2k18.png" class="image"><img src="https://2k18.balccon.org/images/f/f5/BalCCon2k18.png" alt="BalCCon2k18.png" width="400" height="215"></a></div></div><h2><span id="14.7C15.7C16_September_2018.2C_Novi_Sad_Fair_-_Congress_Centre.2C_Hajduk_Veljkova_11.2C_Novi_Sad.2C_Vojvodina.2C_Serbia.2C_Europe.2C_Earth.2C_Milky_Way" class="mw-headline"></span><span id="14.7C15.7C16_September_2018.2C_Novi_Sad_Fair_-_Congress_Centre.2C_Hajduk_Veljkova_11.2C_Novi_Sad.2C_Vojvodina.2C_Serbia.2C_Europe.2C_Earth.2C_Milky_Way" class="mw-headline"></span>14|15|16 September 2018, Novi Sad Fair - Congress Centre, Hajduk Veljkova 11, Novi Sad, Vojvodina, Serbia, Europe, Earth, Milky Way</h2></center><h2><span id="Our_goal" class="mw-headline"></span><span id="Our_goal" class="mw-headline"></span>Our goal</h2><p>Our major goal is to gather all the communities from the region and abroad so we can socialize, hack, play, learn, exchange knowledge and experience and of course, party together.</p><p>BalCCon aims to become the center of the hacker community in the region as well as to provide an opportunity for all the people in this part of Europe to connect and to cooperate.</p><p>Program and speakers will be taken through a “<strong>Call for Papers</strong>” few months before the congress. We have planned some special guests whose reputation is well known in the community. All applications sent by people will be stored and graded by congress orga people.</p><h2><span id="News" class="mw-headline"></span><span id="News" class="mw-headline"></span>News</h2><ul><li>Twitter - <a href="https://twitter.com/BalCC0n" class="external text" rel="nofollow"><span style="color: #ffac2a;">https://twitter.com/BalCC0n</span></a></li></ul><h2><span id="General" class="mw-headline"></span><span id="General" class="mw-headline"></span>General</h2><ul><li>Schedule of the official conference program</li><li><span style="color: #ffac2a;">Tickets – pricing and information on ticket sales</span></li><li><a href="https://2k18.balccon.org/index.php?title=FAQ" title="FAQ"><span style="color: #ffac2a;">Frequently Asked Questions – FAQ</span></a></li></ul><h2><span id="Accommodation_and_Travel_Information" class="mw-headline"></span><span id="Accommodation_and_Travel_Information" class="mw-headline"></span>Accommodation and Travel Information</h2><ul><li><span style="color: #ffac2a;">Accomodation</span></li><li><a href="https://2k18.balccon.org/index.php?title=Travel_map" title="Travel map"><span style="color: #ffac2a;">Travel map</span></a></li></ul>
