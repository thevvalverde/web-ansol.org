---
categories: []
metadata:
  event_location:
  - event_location_value: Sintra
  event_site:
  - event_site_url: https://www.meetup.com/ubuntupt/events/257002212
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2019-04-17 23:00:00.000000000 +01:00
    event_start_value2: 2019-04-17 23:00:00.000000000 +01:00
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 667
layout: evento
title: 'Ubuntu-PT: Lançamento Disco Dingo'
created: 1555429898
date: 2019-04-16
---

