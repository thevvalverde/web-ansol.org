---
categories: []
metadata:
  event_location:
  - event_location_value: Saloon, Sintra
  event_start:
  - event_start_value: 2018-12-06 20:00:00.000000000 +00:00
    event_start_value2: 2018-12-06 20:00:00.000000000 +00:00
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 637
layout: evento
title: Encontro Ubuntu-pt
created: 1543102481
date: 2018-11-24
---
<p>Todos os meses, numa quinta-feira, a comunidade Ubuntu Portugal reúne-se no Saloon, em Sintra.</p><p>Vem, traz um amigo ou um familiar e vem conviver e partilhar experiências com o resto da comunidade portuguesa..</p><p>Mais Informações:<br> Saloon<br> Avenida Movimento das Forças Armadas n 5, 2710-433 Sintra<br> (2 min a pé da estaçao de comboios da portela de Sintra)<br> O wi-fi é grátis.<br> http://www.openstreetmap.org/node/1594158358</p>
