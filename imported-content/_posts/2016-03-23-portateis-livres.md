---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 406
layout: page
title: Portáteis Livres
created: 1458730476
date: 2016-03-23
---
<p>Consideram-se portáteis lívres aqueles que não têm software proprietário (por exemplo, que tenha uma BIOS livre). <br><br>Existe, além disso, o conceito de portáteis "amigos da liberdade": aqueles que, além de serem vendidos livres de software proprietário, todos os seus componentes de hardware são compatíveis com software 100% livre.<br><br>Não havendo, de momento, nenhum portátil assim descrito à venda em Portugal (que tenhamos conhecimento), é ainda assim útil divulgar uma terceira categoria de portáteis: aqueles que são vendidos sem Sistema Operativo ou com um Sistema Operativo Livre.</p><p>Se já comprou um portátil com Sistema Operativo, saiba como <a href="https://wiki.fsfe.org/Migrated/WindowsTaxRefund/Portugal">devolver o SO e obter a devolução do dinheiro</a>.</p><h2>Lojas ou Marcas que vendem portáteis sem Sistema Operativo (ou com um Sistema Operativo Livre) em Portugal</h2><p>A seguinte lista é mantida com regularidade, e lista marcas ou lojas que vendem portáteis sem Sistema Operativo em Portugal. Se conhece mais casos, ou se encontrar algum destes dados desactualizados, por favor contacte-nos para contacto(arroba)ansol.org .</p><h3>Assismatica</h3><p>Os portáteis que vendem sem sistema operativo são os que fazem e assemblam. Os portáteis das marcas (HP, Asus, Toshiba e Acer) têm acordos com a Microsoft e todos eles são vendidos com sistema operativo. <br>Link: <a href="http://www.assismatica.pt" target="_blank" rel="nofollow">http://www.assismatica.pt</a></p><h3>Insys</h3><p>Alguns dos portáteis da InSys são vendidos sem SO, ou com um Sistema Operativo Livre. Podem ser frequentemente encontrados na <a href="http://www.elitedigital.pt/catalogo/index.php?l=ct&amp;cat=1000001&amp;ordem=&amp;marca=10&amp;s=&amp;onlystock=1">Elite Digital</a>.</p>
