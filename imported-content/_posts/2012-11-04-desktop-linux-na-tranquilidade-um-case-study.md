---
excerpt: "<p>Realizar-se-&aacute; no proximo dia 17 de Novembro, pelas 9:00, no Audit&oacute;rio
  1, Edificio ISCTE I, uma sess&atilde;o subordinada ao tema &ldquo;Desktop Linux
  na Tranquilidade &ndash; Um Case Study&rdquo;, no contexto do Mestrado de Software
  de C&oacute;digo Aberto (MOSS). A entrada &eacute; livre.</p>\r\n"
categories: []
metadata:
  event_location:
  - event_location_value: Auditório 1, Edificio ISCTE I, Lisboa
  event_site:
  - event_site_url: http://masteropensource.wordpress.com/2012/11/04/desktop-linux-na-tranquilidade-um-case-study/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2012-11-17 09:00:00.000000000 +00:00
    event_start_value2: 2012-11-17 09:00:00.000000000 +00:00
  node_id: 101
layout: evento
title: Desktop Linux na Tranquilidade – Um Case Study
created: 1352068635
date: 2012-11-04
---
<p>Realizar-se-&aacute; no proximo dia 17 de Novembro, pelas 9:00, no Audit&oacute;rio 1, Edificio ISCTE I, uma sess&atilde;o subordinada ao tema &ldquo;Desktop Linux na Tranquilidade &ndash; Um Case Study&rdquo;, no contexto do Mestrado de Software de C&oacute;digo Aberto (MOSS). A entrada &eacute; livre.</p>
<p>O orador convidado &eacute; Rui Lapa, administrador de sistemas Linux, Integrador de sistemas na Tranqulidade e respons&aacute;vel pelo projecto.</p>
