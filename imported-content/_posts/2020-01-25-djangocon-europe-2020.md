---
categories: []
metadata:
  event_location:
  - event_location_value: Porto
  event_site:
  - event_site_url: https://2020.djangocon.eu/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2020-09-15 23:00:00.000000000 +01:00
    event_start_value2: 2020-09-19 23:00:00.000000000 +01:00
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 729
layout: evento
title: DjangoCon Europe 2020
created: 1579915883
date: 2020-01-25
---

