---
categories:
- hackathon
- mozilla
- floss
- lisboa
metadata:
  event_location:
  - event_location_value: 'Bright Pixel, Rua da emenda n19,1200-169 Lisboa, Portugal '
  event_site:
  - event_site_url: https://ti.to/Mozilla/global-sprint-lisboa18/en
    event_site_title: Global Sprint 2018 Lisboa - Bright Pixel
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2018-05-09 23:00:00.000000000 +01:00
    event_start_value2: 2018-05-10 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 119
  - tags_tid: 306
  - tags_tid: 273
  - tags_tid: 304
  node_id: 616
layout: evento
title: Global Sprint 2018 Lisboa - Bright Pixel
created: 1524223517
date: 2018-04-20
---
<div class="cms"><p><img src="https://assets.mofoprod.net/network/home-%402x.jpg" width="515" height="197"></p><h4 class="h4-light-black">Mozilla’s Global Sprint is a fun, two-day collaborative hackathon. A diverse network of educators, engineers, artists, scientists, and many others come together in person and online to build projects for a healthy Internet.</h4></div><p>As part of a global movement to make the Internet better, safer, and healthier, educators are creating games that explore online privacy and digital citizenship. Scientists are sharing data online to speed the discovery of new medical treatments. Coders are building smartphone apps to prevent gender-based violence. <em><strong>All this (and much more) is happening in the open—so anyone can watch this work evolve, build on shared knowledge, and pitch in to help out. <br></strong></em></p><p class="quote-small"><strong>At the Global Sprint, you can join us… no matter your skill level or background.</strong></p><p><span><strong>We’ll gather to work on over 100 projects </strong><span><strong>to support a healthy Internet.</strong> Bring your design or writing skills, expertise in coding, teaching, QA testing, game design, and more. We’ll use basic online tools like chat, video conferencing, collaborative editing software, and code/content sharing platforms to work and learn together. Collaborate with partners in the same room—or on the other side of the world.</span></span></p>
