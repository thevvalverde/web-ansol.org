---
categories:
- '2013'
- ansol
- drm
metadata:
  event_location:
  - event_location_value: Assembleia da República
  event_site:
  - event_site_url: http://www.parlamento.pt/ActividadeParlamentar/Paginas/DetalheAudiencia.aspx?BID=95688
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2013-07-10 10:00:00.000000000 +01:00
    event_start_value2: 2013-07-10 10:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 32
  - tags_tid: 33
  - tags_tid: 10
  node_id: 183
layout: evento
title: ANSOL ouvida em Audiência sobre DRM
created: 1372957749
date: 2013-07-04
---
<div style="float: left; padding: 1em;"><img src="http://1.bp.blogspot.com/-ryas6hKSvNc/UcIjsanLx3I/AAAAAAAAF-o/UqsHC_4H_G4/s1600/Screen+Shot+2013-06-19+at+10.29.30+PM.png" width="300px"></div><p>A ANSOL e a AEL irão ser ouvidas no Parlamento sobre os Projectos-Lei sobre DRM que visam corrigir os problemas com a actual Legislação.</p><p>Estes Projectos foram aprovados na generalidade, e irão agora ser discutidos e votados em sede de especialidade, pela 1ª Comissão.</p><hr><p>&nbsp;</p><p>Após a aprovação na generalidade dos Projectos-Lei sobre DRM que visam corrigir os problemas com a actual Legislação, estes irão agora ser discutidos e votados em sede de especialidade, pela 1ª Comissão.</p><p>Nesse âmbito, a ANSOL, representada pelo seu Presidente, Rui Seabra, irá ter uma audição conjunta com Paula Simões, da Direcção da Associação Ensino Livre, onde irá expor o seu entendimento quanto à matéria em causa.</p><p>De relembrar que a ANSOL <a href="https://ansol.org/pr-20130610">apelou à Assembleia da República</a> para que aprovasse estes diplomas.</p>
