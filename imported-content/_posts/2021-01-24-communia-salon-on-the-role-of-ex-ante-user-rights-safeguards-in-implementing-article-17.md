---
categories: []
metadata:
  event_location:
  - event_location_value: Online
  event_site:
  - event_site_url: https://www.communia-association.org/2021/01/18/communia-salon-on-the-role-of-ex-ante-user-rights-safeguards-in-implementing-article-17/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2021-01-26 00:00:00.000000000 +00:00
    event_start_value2: 2021-01-26 00:00:00.000000000 +00:00
  mapa:
  - {}
  slide:
  - slide_value: 0
  node_id: 774
layout: evento
title: COMMUNIA salon on the role of ex-ante user rights safeguards in implementing
  Article 17
created: 1611508863
date: 2021-01-24
---

