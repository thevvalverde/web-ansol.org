---
categories:
- consultoria
- formação
- suporte
metadata:
  email:
  - email_email: info@clubedeinfo.org
  servicos:
  - servicos_tid: 7
  - servicos_tid: 3
  - servicos_tid: 2
  site:
  - site_url: http://www.clubedeinfo.org
    site_title: http://www.clubedeinfo.org
    site_attributes: a:0:{}
  node_id: 38
layout: servicos
title: Clube de Informática - suporte p/ redes económicas LTSP, Linux, Open Office
created: 1334499031
date: 2012-04-15
---
Clube de Informatica proporciona instalação e suporte para redes económicas LTSP, com reciclagem de computadores obsoletos redes Linux aplicações: OpenOffice, Mozilla, Thunderbird, Gaim 
