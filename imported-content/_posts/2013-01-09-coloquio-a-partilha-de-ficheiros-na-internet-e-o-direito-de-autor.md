---
categories:
- cópia privada
metadata:
  event_site:
  - event_site_url: http://www.pgr.pt/grupo_soltas/Actualidades/2012/coloquio_internet.html
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2013-01-18 00:00:00.000000000 +00:00
    event_start_value2: 2013-01-18 00:00:00.000000000 +00:00
  tags:
  - tags_tid: 11
  node_id: 117
layout: evento
title: Colóquio - A Partilha de Ficheiros na Internet e o Direito de Autor
created: 1357732731
date: 2013-01-09
---
<p>Nota: fomos informados que a lota&ccedil;&atilde;o para este evento j&aacute; se encontra esgotada.</p>
<table border="0" cellpadding="0" cellspacing="0" height="90" id="AutoNumber4" style="font-size: 12px; text-align: left; background-color: rgb(255, 255, 255); " width="99%">
	<tbody>
		<tr>
			<td height="90" style="font-family: Verdana, Geneva, sans-serif; font-size: 10px; text-align: left; color: rgb(51, 51, 51); " valign="top" width="76%">
				<p align="left"><span class="TITULOS" style="color: rgb(189, 0, 8); font-weight: bold; text-align: left; "><span class="sub-title">Col&oacute;quio - A Partilha de Ficheiros na Internet e o Direito de Autor</span></span></p>
				<p align="justify">A Procuradoria-Geral da Rep&uacute;blica promove um col&oacute;quio sobre a valora&ccedil;&atilde;o jur&iacute;dica da partilha de ficheiros na Internet, quando essa partilha significar viola&ccedil;&atilde;o de direito de autor (em particular na vertente penal). Decorrer&aacute; a 18 de Janeiro de 2013, na Procuradoria Geral da Rep&uacute;blica.</p>
				<p align="justify">Este col&oacute;quio destina-se a impulsionar a reflex&atilde;o do Minist&eacute;rio P&uacute;blico sobre esta tem&aacute;tica, mas est&aacute; aberto &agrave; participa&ccedil;&atilde;o da restante comunidade jur&iacute;dica. Al&eacute;m de pretender propiciar aos magistrados do Minist&eacute;rio P&uacute;blico a possibilidade de consolidar opini&atilde;o sobre esta problem&aacute;tica, a sess&atilde;o tem ainda em vista dar a conhecer &agrave; comunidade a discuss&atilde;o sobre a mesma e antever as perspectivas de evolu&ccedil;&atilde;o futura.</p>
				<p align="justify">Planeiam-se interven&ccedil;&otilde;es de magistrados do Minist&eacute;rio P&uacute;blico, mas tamb&eacute;m de especialistas (t&eacute;cnicos e jur&iacute;dicos) na &aacute;rea. Tendo sido anunciada ac&ccedil;&atilde;o legislativa, planeia-se ainda interven&ccedil;&atilde;o da Secretaria de Estado da Cultura.</p>
			</td>
		</tr>
	</tbody>
</table>
<p>&nbsp;</p>
