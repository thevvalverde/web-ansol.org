---
categories: []
metadata:
  event_location:
  - event_location_value: Aveiro
  event_site:
  - event_site_url: http://linux-presentation-day.pt/#
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2016-04-29 23:00:00.000000000 +01:00
    event_start_value2: 2016-04-29 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 415
layout: evento
title: Linux Presentation Day
created: 1460979252
date: 2016-04-18
---
<p>Linux Presentation Day - Uma chance para conhecer o GNU/Linux e o software livre!</p>
