---
categories:
- distribuição/venda
metadata:
  email:
  - email_email: unifase@netcabo.pt
  servicos:
  - servicos_tid: 8
  site:
  - site_url: http://www.unifase.com
    site_title: http://www.unifase.com
    site_attributes: a:0:{}
  node_id: 54
layout: servicos
title: Unifase Informática
created: 1334501924
date: 2012-04-15
---
<p>Empresa especializada na venda de hardware e software. Destingue-se das suas empresas concorrentes em Coimbra, (fazendo tamb&eacute;m uma cobertura a n&iacute;vel nacional) em distribuir, e dar suporte conforme a op&ccedil;&atilde;o do cliente al&eacute;m de Sistemas Operativos Propriet&aacute;rios, &agrave; distribui&ccedil;&atilde;o de Linux Ubuntu. O cliente pode optar por adquirir um PC de secret&aacute;ria, ou port&aacute;til com a instala&ccedil;&atilde;o da distribui&ccedil;&atilde;o Ubuntu. Nesta mat&eacute;ria caso o utilizador prefira comprar o equipamento inform&aacute;tico sem sistema operativo, ser-lhe-&aacute; oferecido uma c&oacute;pia do Ubuntu para instala&ccedil;&atilde;o pessoal, e personaliza&ccedil;&atilde;o &agrave; sua escolha. &Eacute; o nosso compromisso servir os nosso clientes com o que de melhor &eacute; feito a n&iacute;vel de software gratuito.</p>
