---
categories: []
metadata:
  event_location:
  - event_location_value: Loseley Park, Guildford, UK
  event_site:
  - event_site_url: https://www.emfcamp.org/
    event_site_title: Electromagnetic Field 2016
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2016-08-04 23:00:00.000000000 +01:00
    event_start_value2: 2016-08-06 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 401
layout: evento
title: Electromagnetic Field 2016
created: 1454028174
date: 2016-01-29
---
<p class="emphasis">Electromagnetic Field is a non-profit UK camping festival for those with an inquisitive mind or an interest in making things: hackers, artists, geeks, crafters, scientists, and engineers.</p><p>A temporary town of more than a thousand like-minded people enjoying a long weekend of talks, performances, and workshops on everything from&nbsp;<span>blacksmithing</span>&nbsp;to&nbsp;<span>biometrics</span>,&nbsp;<span>chiptunes</span>&nbsp;to<span>computer security</span>,&nbsp;<span>high altitude ballooning</span>&nbsp;to&nbsp;<span>lockpicking</span>,<span>origami</span>&nbsp;to&nbsp;<span>democracy</span>, and&nbsp;<span>online privacy</span>&nbsp;to&nbsp;<span>knitting</span>.</p><p>To help matters along, we provide&nbsp;<span>fast internet</span>,&nbsp;<span>power to the tent</span>,&nbsp;<span>good beer</span>, and&nbsp;<span>amazing installations</span>, entirely organised by a dedicated team of volunteers.</p><p><span>Electromagnetic Field 2016</span><span>&nbsp;will be held on</span><span>August 5–7 2016</span><span>&nbsp;at&nbsp;</span><span><span>Loseley Park</span>,&nbsp;<span><span>Guildford</span>.</span></span></p>
