---
categories: []
metadata:
  event_location:
  - event_location_value: Bruxelas, BE
  event_site:
  - event_site_url: http://www.opencon2015.org/
    event_site_title: OpenCon 2015
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2015-11-14 00:00:00.000000000 +00:00
    event_start_value2: 2015-11-16 00:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 125
  - tags_tid: 126
  node_id: 362
layout: evento
title: OpenCon 2015
created: 1445731887
date: 2015-10-25
---
<p><span>"The student and early career academic professional conference on Open Access, Open Education, and Open Data"</span><br><span>Empowering the Next Generation to Advance Open Access, Open Education and Open Data will take place in on November 14-16 in Brussels, Belgium and bring together students and early career academic professionals from across the world to learn about the issues, develop critical skills, and return home ready to catalyze action toward a more open system for sharing the world’s information — from scholarly and scientific research, to educational materials, to digital data."</span><br><span>CFP:</span><br><span>&nbsp;&nbsp;&nbsp; * Applications to attend OpenCon are open until June 22nd, but applicants are encouraged to apply early.</span><br><span>&nbsp;&nbsp;&nbsp; * OpenCon seeks to bring together the most capable, motivated students and early career academic professionals from around the world to advance Open Access, Open Education, and Open Data—regardless of their ability to cover travel costs. In 2014, more than 80% of attendees received support. Due to this, attendance at OpenCon is by application only.</span><br><span>&nbsp;&nbsp;&nbsp; * Applicants can request a full or partial travel scholarship, which will be awarded to most of those accepted. OpenCon 2015 will convene students and early career academic professionals from around the world and serve as a powerful catalyst for projects led by the next generation to advance OpenCon’s three focus areas—Open Access, Open Education, and Open Research Data. Through a program of keynotes, panel discussions, workshops, and hackathons, participants will build skills in key areas—from raising institutional awareness to coordinating national-level campaigns effectively. Apply early at www.opencon2015.org/attend."</span><br><span>Co-Org:</span><br><span>&nbsp;&nbsp;&nbsp; Jonathan Gray (OpenPhilosophy)</span></p>
