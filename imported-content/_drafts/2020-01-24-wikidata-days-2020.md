---
categories: []
metadata:
  event_location:
  - event_location_value: Carcavelos
  event_site:
  - event_site_url: https://meta.wikimedia.org/wiki/WikiData_Days_2020
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2020-07-01 23:00:00.000000000 +01:00
    event_start_value2: 2020-07-03 23:00:00.000000000 +01:00
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 726
layout: evento
title: WikiData Days 2020
created: 1579867996
date: 2020-01-24
---
<p>Cancelado por causa do COVID-19.</p>
