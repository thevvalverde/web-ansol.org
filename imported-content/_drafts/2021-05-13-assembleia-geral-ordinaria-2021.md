---
categories: []
metadata:
  event_start:
  - event_start_value: 2021-06-19 13:00:00.000000000 +01:00
    event_start_value2: 2021-06-19 18:00:00.000000000 +01:00
  mapa:
  - {}
  slide:
  - slide_value: 0
  node_id: 800
layout: evento
title: Assembleia Geral Ordinária 2021
created: 1620944350
date: 2021-05-13
---
<p>Convocam-se todos os sócios da ANSOL para a Assembleia Geral que terá lugar no dia 19 de Junho de 2021 pelas 14 horas na sala Jitsi Meet acesível pelo URL <a href="https://..../ANSOL-AG-2021">https://..../ANSOL-AG-2021</a> com a seguinte ordem de trabalhos:<br><br></p><ol><li>Apresentação e aprovação do relatório de contas de 2020</li><li>Apresentação da proposta de plano de trabalhos 2021</li><li>Orçamento 2021</li><li>Outros assuntos</li></ol><p>Se à hora marcada não estiverem presentes, pelo menos, metade dos associados a Assembleia Geral reunirá, em 2ª convocatória, no mesmo local e passados 30 minutos, com qualquer número de presenças.</p><p><br> Data e hora: Sábado, 19 de Junho de 2021 - 14:00</p>
